import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';


export function showWinnerModal(fighter) {
  // call showModal function 
  const imgFighter = createFighterImage(fighter);
  showModal({
    title: `${fighter.name} WON!!!`,
    bodyElement: imgFighter,
    onClose: () => {
      location.reload();
    }
  });
}
